package ovh.bmyguest.baware;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.Toast;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isNotChecked;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testOnLogin_Validate(){
        onView(withId(R.id.text_view_login_username)).perform(typeText("Kevin"));
        onView(withId(R.id.text_view_login_password)).perform(typeText("pouet"));
        onView(withId(R.id.button_login)).perform(click());
        onView(withId(R.id.button_article_create)).check(matches(isDisplayed()));
    }
    @Test
    public void testOnLogin_Error(){
        onView(withId(R.id.text_view_login_username)).perform(typeText("Kevi"));
        onView(withId(R.id.text_view_login_password)).perform(typeText("pouet"));
        onView(withId(R.id.button_login)).perform(click());
        onView(withText("Error 401: Invalid credentials")).inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
    }

    @Test
    public void testOnRegisterButtonPressed_Validate(){
        onView(withId(R.id.text_view_login_username)).perform(typeText("Yoann"));
        onView(withId(R.id.text_view_login_password)).perform(typeText("test"));
        onView(withId(R.id.button_register)).perform(click());
        onView(withId(R.id.button_article_create)).check(matches(isDisplayed()));
    }

    @Test
    public void testOnRegisterButtonPressed_Error(){
        onView(withId(R.id.text_view_login_username)).perform(typeText("Yoann"));
        onView(withId(R.id.text_view_login_password)).perform(typeText("test"));
        onView(withId(R.id.button_register)).perform(click());
        onView(withText("Error 401: Username already in use.")).inRoot(withDecorView(not(is(mActivityRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
    }

    /*@Test
    public void testTwoEditTextEqual(){
        onView(withId(R.id.firstEditText)).perform(typeText("TOTO"));
        onView(withId(R.id.secondEditText)).perform(typeText("TOTO"));
        onView(withId(R.id.validatorButton)).perform(click());

        onView(withId(R.id.resultTextView)).check(matches(withText(SUCCESS_MESSAGE)));
    }*/
}
