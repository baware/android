package ovh.bmyguest.baware;

import org.junit.Test;

import ovh.bmyguest.baware.util.Tools;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class MainActivityUnitTest {

    @Test
    public void testEncodeBase64String(){
        String str = "test";
        assertEquals("dGVzdA==",Tools.encodeBase64String(str));
    }

    @Test
    public void testDecodeBase64String(){
        String str = "dGVzdA==";
        assertEquals("test",Tools.decodeBase64String(str));
    }

    /*@Test
    public void testEquality_FirstString_SecondString_null_notnull(){
        MainActivity mainActivity= new MainActivity();
        String firstString=null;
        String secondString="TOTO";
        boolean result=mainActivity.isTwoStringEqual(firstString, secondString);
        assertFalse(result);
    }*/

}