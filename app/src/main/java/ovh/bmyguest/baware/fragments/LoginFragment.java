package ovh.bmyguest.baware.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import ovh.bmyguest.baware.R;
import ovh.bmyguest.baware.parcelables.User;

public class LoginFragment extends Fragment {
    public static final String ARG_USERNAME = "USERNAME";
    public static final String ARG_PASSWORD = "PASSWORD";
    private String username;
    private String password;
    private TextView passwordTextView;
    private TextView usernameTextView;
    private OnLoginInteractionsListener listener;

    public interface OnLoginInteractionsListener {
        void onLoginButtonPressed(User user, boolean redirectLogin);
        void onRegisterButtonPressed(User user);
    }

    public static LoginFragment newInstance(String username, String password) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USERNAME, username);
        args.putString(ARG_PASSWORD, password);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        username = getArguments() != null ? getArguments().getString(ARG_USERNAME) : null;
        password = getArguments() != null ? getArguments().getString(ARG_PASSWORD) : null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.fragment_login_title);
            actionBar.hide();

            try {
                actionBar.getClass().getDeclaredMethod("setShowHideAnimationEnabled", boolean.class).invoke(actionBar, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        usernameTextView = view.findViewById(R.id.text_view_login_username);
        usernameTextView.setText(username);
        passwordTextView = view.findViewById(R.id.text_view_login_password);
        passwordTextView.setText(password);

        Button buttonLogin = view.findViewById(R.id.button_login);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User(usernameTextView.getText().toString(), passwordTextView.getText().toString());
                listener.onLoginButtonPressed(user, false);
            }
        });

        Button buttonRegister = view.findViewById(R.id.button_register);
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User(usernameTextView.getText().toString(), passwordTextView.getText().toString());
                listener.onRegisterButtonPressed(user);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginInteractionsListener) {
            listener = (OnLoginInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnLoginInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
