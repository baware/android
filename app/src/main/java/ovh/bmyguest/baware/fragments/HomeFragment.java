package ovh.bmyguest.baware.fragments;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import ovh.bmyguest.baware.R;
import ovh.bmyguest.baware.adapters.ArticleListAdapter;
import ovh.bmyguest.baware.parcelables.Article;
import ovh.bmyguest.baware.services.MessagingService;

import static android.content.Context.NOTIFICATION_SERVICE;

public class HomeFragment extends ListFragment {
    public final static int NOTIFICATION_TYPE_ARTICLE = 666;
    private OnArticleInteractionsListener listener;
    public ArticleListAdapter articlesListAdapter;
    private SwipeRefreshLayout articlesSwipeRefreshLayout;
    public FloatingActionButton buttonCreate;

    public interface OnArticleInteractionsListener {
        void onArticleSelected(Article article, int position);
        void onEditArticleButtonPressed(Article article, int position);
        void onDeleteArticleButtonPressed(Article article);
        void onCreateArticleButtonPressed();
        void onLogout();
        ArrayList<Article> getArticlesList();
        void loadArticles();
        void copyToClipboard(String str);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (MessagingService.notifications.containsKey(String.valueOf(NOTIFICATION_TYPE_ARTICLE))) {
            MessagingService.notifications.remove(String.valueOf(NOTIFICATION_TYPE_ARTICLE));
            ((NotificationManager) getContext().getSystemService(NOTIFICATION_SERVICE)).cancel(NOTIFICATION_TYPE_ARTICLE);
        }

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.show();
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setTitle(R.string.fragment_home_title);
        }

        articlesSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout_articles);
        articlesSwipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);
        articlesSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listener.loadArticles();
            }
        });

        buttonCreate = view.findViewById(R.id.button_article_create);
        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onCreateArticleButtonPressed();
            }
        });

        setListAdapter(new ArticleListAdapter(getActivity(), listener.getArticlesList()));
        articlesListAdapter = (ArticleListAdapter) getListAdapter();
        listener.loadArticles();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnArticleInteractionsListener) {
            listener = (OnArticleInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnArticleInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_home, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                listener.onLogout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Article article = ((Article) getListAdapter().getItem(position));
        listener.onArticleSelected(article, position);
    }

    public void displayArticles() {
        ((ArticleListAdapter) getListAdapter()).notifyDataSetChanged();
        articlesSwipeRefreshLayout.setRefreshing(false);
    }
}
