package ovh.bmyguest.baware.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.Date;

import io.github.rockerhieu.emojicon.EmojiconEditText;
import ovh.bmyguest.baware.R;
import ovh.bmyguest.baware.parcelables.Comment;
import ovh.bmyguest.baware.util.Tools;

public class CommentEditFragment extends Fragment {
    public static final String ARG_COMMENT = "comment";
    public static final String ARG_COMMENT_POSITION = "comment_position";
    public Comment comment;
    public int position;
    private EmojiconEditText commentContentEditText;
    private OnCommentEditInteractionsListener listener;

    public interface OnCommentEditInteractionsListener {
        void onCommentEditConfirm(Comment comment, int position);
        void closeKeyboard();
    }

    public static CommentEditFragment newInstance(Comment comment, int position) {
        CommentEditFragment fragment = new CommentEditFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_COMMENT, comment);
        args.putInt(ARG_COMMENT_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        comment = getArguments() != null ? (Comment) getArguments().getParcelable(ARG_COMMENT) : null;
        position = getArguments() != null ? getArguments().getInt(ARG_COMMENT_POSITION) : -1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_comment_edit, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.fragment_comment_edit_title);
        }

        commentContentEditText = view.findViewById(R.id.edit_text_emojicon);
        commentContentEditText.setText(Tools.decodeBase64String(comment.getContent()));
        commentContentEditText.requestFocus();
        commentContentEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    listener.closeKeyboard();
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCommentEditInteractionsListener) {
            listener = (OnCommentEditInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnCommentEditInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_comment_edit, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_confirm:
                comment.setContent(Tools.encodeBase64String(commentContentEditText.getText().toString()));
                comment.setUpdatedAt(new Date());
                listener.onCommentEditConfirm(comment, position);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
