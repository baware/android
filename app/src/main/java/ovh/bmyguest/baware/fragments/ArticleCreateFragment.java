package ovh.bmyguest.baware.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Date;

import io.github.rockerhieu.emojicon.EmojiconEditText;
import ovh.bmyguest.baware.R;
import ovh.bmyguest.baware.parcelables.Article;
import ovh.bmyguest.baware.parcelables.Category;
import ovh.bmyguest.baware.util.Tools;

public class ArticleCreateFragment extends Fragment {
    private EmojiconEditText articleTitleEditText;
    private EmojiconEditText articleHeadlinesEditText;
    private EditText articlePictureUrlEditText;
    private EmojiconEditText articleContentEditText;
    private Spinner articleCategorySpinner;
    private OnArticleCreateInteractionsListener listener;

    public interface OnArticleCreateInteractionsListener {
        void onPostArticleButtonPressed(Article article);
        void loadCategories();
        ArrayList<Category> getCategoriesList();
    }

    public static ArticleCreateFragment newInstance() {
        return new ArticleCreateFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_article_create, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.fragment_article_create_title);
        }

        articleHeadlinesEditText = view.findViewById(R.id.edit_text_emojicon_headlines);
        articlePictureUrlEditText = view.findViewById(R.id.edit_text_picture_url);
        articleContentEditText = view.findViewById(R.id.edit_text_emojicon_content);
        articleCategorySpinner = view.findViewById(R.id.spinner_category);
        articleCategorySpinner.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, listener.getCategoriesList()));
        articleTitleEditText = view.findViewById(R.id.edit_text_emojicon_title);
        articleTitleEditText.requestFocus();

        listener.loadCategories();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnArticleCreateInteractionsListener) {
            listener = (OnArticleCreateInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnArticleCreateInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_article_create, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_confirm:
                Article article = new Article(Tools.encodeBase64String(articleTitleEditText.getText().toString()), Tools.encodeBase64String(articleHeadlinesEditText.getText().toString()), Tools.encodeBase64String(articleContentEditText.getText().toString()), articlePictureUrlEditText.getText().toString(), new Date(), new Date(), 0, null, (Category) articleCategorySpinner.getSelectedItem());
                listener.onPostArticleButtonPressed(article);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
