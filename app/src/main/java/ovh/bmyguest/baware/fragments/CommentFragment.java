package ovh.bmyguest.baware.fragments;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Date;

import io.github.rockerhieu.emojicon.EmojiconEditText;
import ovh.bmyguest.baware.R;
import ovh.bmyguest.baware.adapters.CommentListAdapter;
import ovh.bmyguest.baware.parcelables.Article;
import ovh.bmyguest.baware.parcelables.Comment;
import ovh.bmyguest.baware.services.MessagingService;
import ovh.bmyguest.baware.util.Tools;

import static android.content.Context.NOTIFICATION_SERVICE;

public class CommentFragment extends ListFragment {
    public final static int NOTIFICATION_TYPE_ARTICLE = 666;
    public final static int NOTIFICATION_TYPE_COMMENT = 667;
    public static final String ARG_ARTICLE = "ARTICLE";
    public static final String ARG_SCROLL_TO_LAST = "SCROLL_TO_LAST";
    public Article article;
    public boolean scrollToLast;
    private OnCommentInteractions listener;
    private Button buttonSend;
    private SwipeRefreshLayout commentsSwipeRefreshLayout;
    private EmojiconEditText commentEditText;

    public interface OnCommentInteractions {
        void onPostCommentButtonPressed(Comment comment);
        void onDeleteCommentButtonPressed(Comment comment);
        void onEditCommentButtonPressed(Comment comment, int position);
        void onCommentVote(Comment comment, int note);
        void loadArticleComments(Article article, final boolean scrollToLast);
        void closeKeyboard();
        void copyToClipboard(String str);
    }

    public static CommentFragment newInstance(Article article, boolean scrollToLast) {
        CommentFragment fragment = new CommentFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_ARTICLE, article);
        args.putBoolean(ARG_SCROLL_TO_LAST, scrollToLast);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            article = getArguments().getParcelable(ARG_ARTICLE);
            scrollToLast = getArguments().getBoolean(ARG_SCROLL_TO_LAST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_comment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (MessagingService.notifications.containsKey(article.getId() + "" + NOTIFICATION_TYPE_COMMENT)) {
            MessagingService.notifications.remove(article.getId() + "" + NOTIFICATION_TYPE_COMMENT);
            ((NotificationManager) getContext().getSystemService(NOTIFICATION_SERVICE)).cancel(Integer.parseInt(article.getId() + "" + NOTIFICATION_TYPE_ARTICLE));
        }

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.fragment_comment_title);
        }

        commentsSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout_comments);
        commentsSwipeRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);
        commentsSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listener.loadArticleComments(article, false);
            }
        });

        buttonSend = view.findViewById(R.id.send_button);
        buttonSend.setFocusable(false);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Comment comment = new Comment(Tools.encodeBase64String(commentEditText.getText().toString()), null, new Date(), new Date(), 0, null);
                commentEditText.setText("");
                listener.onPostCommentButtonPressed(comment);
            }
        });

        commentEditText = view.findViewById(R.id.editEmojicon);
        commentEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                buttonSend.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
            }
        });

        commentEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    listener.closeKeyboard();
                }
            }
        });

        setListAdapter(new CommentListAdapter(getActivity(), article.getComments(), article));
        displayArticleComments(false);
        listener.loadArticleComments(article, scrollToLast);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCommentInteractions) {
            listener = (OnCommentInteractions) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnCommentInteractions");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public void displayArticleComments(boolean scrollToLast) {
        ((CommentListAdapter) getListAdapter()).notifyDataSetChanged();
        commentsSwipeRefreshLayout.setRefreshing(false);

        if (getListAdapter().getCount() > 0 && scrollToLast) {
            getListView().setSelection(getListAdapter().getCount() - 1);
        }
    }
}
