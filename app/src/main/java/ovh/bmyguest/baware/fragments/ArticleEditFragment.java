package ovh.bmyguest.baware.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Date;

import io.github.rockerhieu.emojicon.EmojiconEditText;
import ovh.bmyguest.baware.R;
import ovh.bmyguest.baware.parcelables.Article;
import ovh.bmyguest.baware.parcelables.Category;
import ovh.bmyguest.baware.util.Tools;

public class ArticleEditFragment extends Fragment {
    public static final String ARG_ARTICLE = "article";
    public static final String ARG_ARTICLE_POSITION = "article_position";
    public Article article;
    public int position;
    private EmojiconEditText articleTitleEditText;
    private EmojiconEditText articleHeadlinesEditText;
    private EditText articlePictureUrlEditText;
    private EmojiconEditText articleContentEditText;
    private Spinner articleCategorySpinner;
    private OnArticleEditInteractionsListener listener;

    public interface OnArticleEditInteractionsListener {
        void onArticleEditConfirm(Article article, int position);
        void loadCategories();
        ArrayList<Category> getCategoriesList();
    }

    public static ArticleEditFragment newInstance(Article article, int position) {
        ArticleEditFragment fragment = new ArticleEditFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_ARTICLE, article);
        args.putInt(ARG_ARTICLE_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        article = getArguments() != null ? (Article) getArguments().getParcelable(ARG_ARTICLE) : null;
        position = getArguments() != null ? getArguments().getInt(ARG_ARTICLE_POSITION) : -1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_article_create, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.fragment_article_edit_title);
        }

        articleCategorySpinner = view.findViewById(R.id.spinner_category);
        articleTitleEditText = view.findViewById(R.id.edit_text_emojicon_title);
        articleHeadlinesEditText = view.findViewById(R.id.edit_text_emojicon_headlines);
        articlePictureUrlEditText = view.findViewById(R.id.edit_text_picture_url);
        articleContentEditText = view.findViewById(R.id.edit_text_emojicon_content);

        display();
        listener.loadCategories();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnArticleEditInteractionsListener) {
            listener = (OnArticleEditInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnArticleEditInteractionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_article_edit, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_confirm:
                articleUpdateConfirm();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void display() {
        articleCategorySpinner.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, listener.getCategoriesList()));
        articleCategorySpinner.setSelection(article.getCategory().getId() - 1);
        articleTitleEditText.setText(Tools.decodeBase64String(article.getTitle()));
        articleTitleEditText.requestFocus();
        articleHeadlinesEditText.setText(Tools.decodeBase64String(article.getHeadlines()));
        articlePictureUrlEditText.setText(article.getPictureUrl());
        articleContentEditText.setText(Tools.decodeBase64String(article.getContent()));
    }

    private void articleUpdateConfirm() {
        article.setTitle(Tools.encodeBase64String(articleTitleEditText.getText().toString()));
        article.setHeadlines(Tools.encodeBase64String(articleHeadlinesEditText.getText().toString()));
        article.setPictureUrl(articlePictureUrlEditText.getText().toString());
        article.setContent(Tools.encodeBase64String(articleContentEditText.getText().toString()));
        article.setCategory((Category) articleCategorySpinner.getSelectedItem());
        article.setUpdatedAt(new Date());

        listener.onArticleEditConfirm(article, position);
    }
}
