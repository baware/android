package ovh.bmyguest.baware.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.text.SimpleDateFormat;
import java.util.Locale;

import ovh.bmyguest.baware.R;
import ovh.bmyguest.baware.network.ApiRequestManager;
import ovh.bmyguest.baware.parcelables.Article;
import ovh.bmyguest.baware.parcelables.User;
import ovh.bmyguest.baware.util.Tools;

public class ArticleFragment extends Fragment {
    public static final String ARG_ARTICLE = "ARTICLE";
    public static final String ARG_ARTICLE_ID = "ARTICLE_ID";
    public static final String ARG_ARTICLE_POSITION = "POSITION";
    public Article article;
    public int articleId;
    public int position;
    private Button buttonUpVote;
    private Button buttonDownVote;
    private Button buttonComments;
    private OnCommentButtonPressedListener listener;
    private TextView articleTitleTextView;
    private TextView articleDetailsTextView;
    private TextView articleHeadlinesTextView;
    private NetworkImageView articlePictureTextView;
    private TextView articleContentTextView;
    private TextView articleCategoryTextView;
    private TextView articleNoteTextView;

    public interface OnCommentButtonPressedListener {
        User getCurrentUser();
        void onCommentButtonPressed(Article article);
        void onArticleVote(Article article, int vote);
        void onEditArticleButtonPressed(Article article, int position);
        void onDeleteArticleButtonPressed(Article article);
        boolean onSupportNavigateUp();
        void copyToClipboard(String str);
        void loadArticle(Article article);
    }

    public static ArticleFragment newInstance(int articleId, int position) {
        ArticleFragment fragment = new ArticleFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_ARTICLE_ID, articleId);
        args.putInt(ARG_ARTICLE_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    public static ArticleFragment newInstance(Article article, int position) {
        ArticleFragment fragment = new ArticleFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_ARTICLE, article);
        args.putInt(ARG_ARTICLE_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        article = getArguments() != null ? (Article) getArguments().getParcelable(ARG_ARTICLE) : null;
        articleId = getArguments() != null ? getArguments().getInt(ARG_ARTICLE_ID) : -1;
        position = getArguments() != null ? getArguments().getInt(ARG_ARTICLE_POSITION) : -1;

        if (article != null && article.getUser() != null && article.getUser().getId() == listener.getCurrentUser().getId()) {
            setHasOptionsMenu(true);
        }

        if (article == null && articleId != -1) {
            article = new Article();
            article.setId(articleId);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_article, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.fragment_article_title);
        }

        articleTitleTextView = view.findViewById(R.id.text_view_article_title);
        articleDetailsTextView = view.findViewById(R.id.text_view_article_details);
        articleHeadlinesTextView = view.findViewById(R.id.text_view_article_headlines);
        articlePictureTextView = view.findViewById(R.id.image_view_article_picture);
        articleContentTextView = view.findViewById(R.id.text_view_article_content);
        articleCategoryTextView = view.findViewById(R.id.text_view_article_categories);
        articleNoteTextView = view.findViewById(R.id.text_view_article_note);

        buttonComments = view.findViewById(R.id.button_article_comments);
        buttonComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCommentButtonPressed(article);
            }
        });

        buttonUpVote = view.findViewById(R.id.button_article_upvote);
        buttonUpVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onArticleVote(article, 1);
            }
        });

        buttonDownVote = view.findViewById(R.id.button_article_downvote);
        buttonDownVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onArticleVote(article, -1);
            }
        });

        displayArticle();
        listener.loadArticle(article);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCommentButtonPressedListener) {
            listener = (OnCommentButtonPressedListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnCommentButtonPressedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_article, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                listener.onEditArticleButtonPressed(article, position);
                return true;
            case R.id.action_delete:
                listener.onDeleteArticleButtonPressed(article);
                listener.onSupportNavigateUp();
                return true;
            case R.id.action_copy:
                listener.copyToClipboard(Tools.decodeBase64String(article.getTitle()) + "\r\n\r\n" + Tools.decodeBase64String(article.getHeadlines()) + "\r\n\r\n" + Tools.decodeBase64String(article.getContent()));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void displayArticle() {
        articleTitleTextView.setText(article.getTitle() != null ? Tools.decodeBase64String(article.getTitle()) : "");
        articleHeadlinesTextView.setText(article.getHeadlines() != null ? Tools.decodeBase64String(article.getHeadlines()) : "");
        articlePictureTextView.setImageUrl(article.getPictureUrl() != null ? article.getPictureUrl() : "", ApiRequestManager.getInstance(getContext()).getImageLoader());
        articleContentTextView.setText(article.getContent() != null ? Tools.decodeBase64String(article.getContent()) : "");
        articleCategoryTextView.setText(article.getCategory() != null ? article.getCategory().getName() : "");
        buttonComments.setText(article.getComments() != null ? getResources().getQuantityString(R.plurals.fragment_article_comments_text_button, article.getComments().size(), article.getComments().size()) : "");
        articleNoteTextView.setText(article.getEvaluations() != null ? String.valueOf(article.getNote()) : "");
        articleNoteTextView.setTextColor(article.getNote() < 0 ? ContextCompat.getColor(getActivity(), R.color.vote_red) : article.getNote() > 0 ? ContextCompat.getColor(getActivity(), R.color.vote_green) : ContextCompat.getColor(getActivity(), R.color.text_color_dark));

        if (article.getUpdatedAt() != null && article.getUser() != null) {
            SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            SimpleDateFormat formatHour = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
            String updatedAtDate = formatDate.format(article.getUpdatedAt());
            String updatedAtHour = formatHour.format(article.getUpdatedAt());
            articleDetailsTextView.setText(getString(R.string.fragment_article_details, article.getUser().getUsername(), updatedAtDate, updatedAtHour));
        }

        switch (article.hasVoted(listener.getCurrentUser().getId())) {
            case 0:
                buttonUpVote.setBackgroundResource(R.drawable.ic_thumb_up);
                buttonDownVote.setBackgroundResource(R.drawable.ic_thumb_down);
                break;
            case 1:
                buttonUpVote.setBackgroundResource(R.drawable.ic_thumb_up_colored);
                buttonDownVote.setBackgroundResource(R.drawable.ic_thumb_down);
                break;
            case -1:
                buttonDownVote.setBackgroundResource(R.drawable.ic_thumb_down_colored);
                buttonUpVote.setBackgroundResource(R.drawable.ic_thumb_up);
                break;
        }
    }
}
