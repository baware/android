package ovh.bmyguest.baware.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.google.firebase.messaging.FirebaseMessaging;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import ovh.bmyguest.baware.R;
import ovh.bmyguest.baware.fragments.CommentFragment;
import ovh.bmyguest.baware.network.ApiRequestManager;
import ovh.bmyguest.baware.parcelables.Article;
import ovh.bmyguest.baware.parcelables.Comment;
import ovh.bmyguest.baware.util.Tools;

public class CommentListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Comment> commentsList;
    private Article article;
    private CommentFragment.OnCommentInteractions listener;

    public CommentListAdapter(Context context, ArrayList<Comment> commentsList, Article article) {
        this.context = context;
        this.commentsList = commentsList;
        this.article = article;

        if (context instanceof CommentFragment.OnCommentInteractions) {
            listener = (CommentFragment.OnCommentInteractions) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnCommentInteractions");
        }
    }

    @Override
    public int getCount() {
        return commentsList.size();
    }

    @Override
    public Object getItem(int position) {
        return commentsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_list_view_row_comment, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Comment comment = (Comment) getItem(position);
        int userId = context.getSharedPreferences(context.getString(R.string.general_preferences), Context.MODE_PRIVATE).getInt(context.getString(R.string.general_preferences_id), 0);

        switch (comment.hasVoted(userId)) {
            case 0:
                viewHolder.buttonUpVote.setBackgroundResource(R.drawable.ic_thumb_up);
                viewHolder.buttonDownVote.setBackgroundResource(R.drawable.ic_thumb_down);
                break;
            case 1:
                viewHolder.buttonUpVote.setBackgroundResource(R.drawable.ic_thumb_up_colored);
                viewHolder.buttonDownVote.setBackgroundResource(R.drawable.ic_thumb_down);
                break;
            case -1:
                viewHolder.buttonDownVote.setBackgroundResource(R.drawable.ic_thumb_down_colored);
                viewHolder.buttonUpVote.setBackgroundResource(R.drawable.ic_thumb_up);
                break;
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
        String updatedAt = simpleDateFormat.format(comment.getUpdatedAt());

        viewHolder.commentUsername.setText(comment.getUser().getUsername());
        viewHolder.commentDetails.setText(updatedAt);
        viewHolder.commentContent.setText(Tools.decodeBase64String(comment.getContent()));
        viewHolder.commentNote.setText(String.valueOf(comment.getNote()));
        viewHolder.commentNote.setTextColor(article.getNote() < 0 ? ContextCompat.getColor(context, R.color.vote_red) : article.getNote() > 0 ? ContextCompat.getColor(context, R.color.vote_green) : ContextCompat.getColor(context, R.color.text_color_dark));
        viewHolder.commentPicture.setImageUrl(comment.getPictureUrl(), ApiRequestManager.getInstance(context).getImageLoader());
        viewHolder.commentActionsMenu.setFocusable(false);

        viewHolder.buttonUpVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCommentVote(comment, 1);
            }
        });

        viewHolder.buttonDownVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCommentVote(comment, -1);
            }
        });

        if (comment.getUser().getId() != userId) {
            viewHolder.commentActionsMenu.setVisibility(View.GONE);
            return convertView;
        }

        FirebaseMessaging.getInstance().subscribeToTopic("Article" + article.getId());
        viewHolder.commentActionsMenu.setVisibility(View.VISIBLE);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, Arrays.asList(context.getResources().getStringArray(R.array.fragment_comment_action_menu))) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent)
            {
                View view;

                if (position == 0) {
                    TextView textView = new TextView(getContext());
                    textView.setHeight(0);
                    textView.setVisibility(View.GONE);
                    view = textView;
                } else {
                    view = super.getDropDownView(position, null, parent);
                }

                parent.setVerticalScrollBarEnabled(false);
                return view;
            }
        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        viewHolder.commentActionsMenu.setAdapter(adapter);
        viewHolder.commentActionsMenu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView)view).setText(null);

                switch(i) {
                    case 1:
                        listener.onEditCommentButtonPressed(comment, position);
                        break;
                    case 2:
                        listener.onDeleteCommentButtonPressed(comment);
                        break;
                    case 3:
                        listener.copyToClipboard(Tools.decodeBase64String(comment.getContent()));
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return convertView;
    }

    private class ViewHolder {
        TextView commentUsername;
        TextView commentDetails;
        TextView commentContent;
        TextView commentNote;
        Button buttonUpVote;
        Button buttonDownVote;
        NetworkImageView commentPicture;
        Spinner commentActionsMenu;

        ViewHolder(View view) {
            commentUsername = view.findViewById(R.id.text_view_comment_username);
            commentDetails = view.findViewById(R.id.text_view_comment_details);
            commentContent = view.findViewById(R.id.text_view_comment_content);
            commentNote = view.findViewById(R.id.text_view_comment_note);
            buttonUpVote = view.findViewById(R.id.button_comment_upvote);
            buttonDownVote = view.findViewById(R.id.button_comment_downvote);
            commentPicture = view.findViewById(R.id.image_view_comment_picture);
            commentActionsMenu = view.findViewById(R.id.spinner_comment_actions_menu);
        }
    }
}
