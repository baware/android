package ovh.bmyguest.baware.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.Arrays;

import ovh.bmyguest.baware.R;
import ovh.bmyguest.baware.fragments.HomeFragment;
import ovh.bmyguest.baware.network.ApiRequestManager;
import ovh.bmyguest.baware.parcelables.Article;
import ovh.bmyguest.baware.util.Tools;

public class ArticleListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Article> articlesList;
    private HomeFragment.OnArticleInteractionsListener listener;

    public ArticleListAdapter(Context context, ArrayList<Article> articlesList) {
        this.context = context;
        this.articlesList = articlesList;

        if (context instanceof HomeFragment.OnArticleInteractionsListener) {
            listener = (HomeFragment.OnArticleInteractionsListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnArticleInteractionsListener");
        }
    }

    @Override
    public int getCount() {
        return articlesList.size();
    }

    @Override
    public Object getItem(int position) {
        return articlesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_list_view_row_article_short, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Article article = (Article) getItem(position);
        viewHolder.articleTitle.setText(Tools.decodeBase64String(article.getTitle()));
        viewHolder.articleHeadlines.setText(Tools.decodeBase64String(article.getHeadlines()));
        viewHolder.articlePicture.setImageUrl(article.getPictureUrl(), ApiRequestManager.getInstance(context).getImageLoader());
        viewHolder.articleActionsMenu.setFocusable(false);
        viewHolder.articleNote.setText(context.getString(R.string.fragment_home_article_note, article.getNote()));
        viewHolder.articleNote.setTextColor(article.getNote() < 0 ? ContextCompat.getColor(context, R.color.vote_red) : article.getNote() > 0 ? ContextCompat.getColor(context, R.color.vote_green) : ContextCompat.getColor(context, R.color.text_color_dark));

        int userId = context.getSharedPreferences(context.getString(R.string.general_preferences), Context.MODE_PRIVATE).getInt(context.getString(R.string.general_preferences_id), 0);
        if (article.getUser().getId() != userId) {
            viewHolder.articleActionsMenu.setVisibility(View.GONE);
            return convertView;
        }

        FirebaseMessaging.getInstance().subscribeToTopic("Article" + article.getId());
        viewHolder.articleActionsMenu.setVisibility(View.VISIBLE);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, Arrays.asList(context.getResources().getStringArray(R.array.fragment_comment_action_menu))) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent)
            {
                View view;

                if (position == 0) {
                    TextView textView = new TextView(getContext());
                    textView.setHeight(0);
                    textView.setVisibility(View.GONE);
                    view = textView;
                } else {
                    view = super.getDropDownView(position, null, parent);
                }

                parent.setVerticalScrollBarEnabled(false);
                return view;
            }
        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        viewHolder.articleActionsMenu.setAdapter(adapter);
        viewHolder.articleActionsMenu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView)view).setText(null);

                switch(i) {
                    case 1:
                        listener.onEditArticleButtonPressed(article, position);
                        break;
                    case 2:
                        listener.onDeleteArticleButtonPressed(article);
                        break;
                    case 3:
                        listener.copyToClipboard(Tools.decodeBase64String(article.getTitle()) + "\r\n\r\n" + Tools.decodeBase64String(article.getHeadlines()) + "\r\n\r\n" + Tools.decodeBase64String(article.getContent()));
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return convertView;
    }

    private class ViewHolder {
        TextView articleTitle;
        TextView articleHeadlines;
        TextView articleNote;
        Spinner articleActionsMenu;
        NetworkImageView articlePicture;

        ViewHolder(View view) {
            articleTitle = view.findViewById(R.id.text_view_article_title);
            articleHeadlines = view.findViewById(R.id.text_view_article_headlines);
            articleNote = view.findViewById(R.id.text_view_article_note);
            articleActionsMenu = view.findViewById(R.id.spinner_articles_actions_menu);
            articlePicture = view.findViewById(R.id.image_view_article_picture);
        }
    }
}
