package ovh.bmyguest.baware.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ovh.bmyguest.baware.MainActivity;
import ovh.bmyguest.baware.R;
import ovh.bmyguest.baware.fragments.ArticleFragment;
import ovh.bmyguest.baware.fragments.CommentFragment;
import ovh.bmyguest.baware.parcelables.Article;
import ovh.bmyguest.baware.util.Tools;

public class MessagingService extends FirebaseMessagingService {
    public static Map<String, ArrayList<String>> notifications = new HashMap<>();
    public static int maxLines = 5;
    public final static int NOTIFICATION_TYPE_ARTICLE = 666;
    public final static int NOTIFICATION_TYPE_COMMENT = 667;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();
        RemoteMessage.Notification notification = remoteMessage.getNotification();
        SharedPreferences userPref = getSharedPreferences(getString(R.string.general_preferences), Context.MODE_PRIVATE);

        if (userPref == null || data == null || userPref.getInt(getString(R.string.general_preferences_id), 0) == Integer.parseInt(data.get("userId"))) {
            return;
        }

        if (!notifications.containsKey(notification.getTag())) {
            notifications.put(notification.getTag(), new ArrayList<String>());
        }

        String message = Integer.parseInt(data.get("type")) == NOTIFICATION_TYPE_ARTICLE ? notification.getTitle() : notification.getBody();

        ArrayList<String> lines;
        lines = notifications.get(notification.getTag());
        lines.add(message);

        String text = Integer.parseInt(data.get("type")) == NOTIFICATION_TYPE_ARTICLE ? getResources().getQuantityString(R.plurals.notification_new_articles, lines.size(), lines.size()) : getResources().getQuantityString(R.plurals.notification_new_comments, lines.size(), lines.size());
        String fragmentName = Integer.parseInt(data.get("type")) == NOTIFICATION_TYPE_ARTICLE ? ArticleFragment.class.getSimpleName() : CommentFragment.class.getSimpleName();

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        NotificationManager nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
            .setSmallIcon(R.drawable.ic_stat_ic_notif)
            .setContentTitle(data.get("category"))
            .setAutoCancel(true)
            .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setContentText(text)
            .setLargeIcon(Tools.getBitmapFromURL(data.get("pictureUrl")));

        for (int i = 0; i < Math.min(lines.size(), maxLines); i++) {
            inboxStyle.addLine(lines.get(i));
        }

        int extraLines = lines.size() - maxLines;
        if (extraLines > 0) {
            String summaryText = Integer.parseInt(data.get("type")) == NOTIFICATION_TYPE_ARTICLE ? getResources().getQuantityString(R.plurals.notification_more_articles, extraLines, extraLines) : getResources().getQuantityString(R.plurals.notification_more_comments, extraLines, extraLines);
            inboxStyle.setSummaryText(summaryText);
        }

        inboxStyle.setBigContentTitle(data.get("category"));
        builder.setStyle(inboxStyle);

        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.setAction(Long.toString(System.currentTimeMillis()));

        Article article = new Article();
        article.setTitle(Tools.encodeBase64String(notification.getTitle()));
        article.setPictureUrl(data.get("pictureUrl"));
        article.setId(Integer.parseInt(data.get("articleId")));

        if (Integer.parseInt(data.get("type")) == NOTIFICATION_TYPE_ARTICLE) {
            article.setHeadlines(Tools.encodeBase64String(notification.getBody()));
        } else if (Integer.parseInt(data.get("type")) == NOTIFICATION_TYPE_COMMENT) {
            resultIntent.putExtra("scroll_to_last", true);
        }

        resultIntent.putExtra("navigate", fragmentName);
        resultIntent.putExtra("article", article);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);

        nManager.notify(Integer.parseInt(notification.getTag()), builder.build());
    }
}
