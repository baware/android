package ovh.bmyguest.baware;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import ovh.bmyguest.baware.adapters.CommentListAdapter;
import ovh.bmyguest.baware.fragments.ArticleCreateFragment;
import ovh.bmyguest.baware.fragments.ArticleEditFragment;
import ovh.bmyguest.baware.fragments.ArticleFragment;
import ovh.bmyguest.baware.fragments.CommentEditFragment;
import ovh.bmyguest.baware.fragments.CommentFragment;
import ovh.bmyguest.baware.fragments.HomeFragment;
import ovh.bmyguest.baware.fragments.LoginFragment;
import ovh.bmyguest.baware.network.ApiRequestManager;
import ovh.bmyguest.baware.network.GsonRequest;
import ovh.bmyguest.baware.parcelables.Article;
import ovh.bmyguest.baware.parcelables.Auth;
import ovh.bmyguest.baware.parcelables.Category;
import ovh.bmyguest.baware.parcelables.Comment;
import ovh.bmyguest.baware.parcelables.Evaluation;
import ovh.bmyguest.baware.parcelables.User;
import ovh.bmyguest.baware.util.Tools;

public class MainActivity extends AppCompatActivity implements
        LoginFragment.OnLoginInteractionsListener,
        HomeFragment.OnArticleInteractionsListener,
        ArticleFragment.OnCommentButtonPressedListener,
        ArticleCreateFragment.OnArticleCreateInteractionsListener,
        ArticleEditFragment.OnArticleEditInteractionsListener,
        CommentFragment.OnCommentInteractions,
        CommentEditFragment.OnCommentEditInteractionsListener
{
    public final static String EXTRA_NAVIGATE = "EXTRA_NAVIGATE";
    private ArrayList<Article> articlesList;
    private ArrayList<Category> categoriesList;
    private User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        articlesList = new ArrayList<>();

        categoriesList = new ArrayList<>();
        categoriesList.add(new Category(1, "Science"));
        categoriesList.add(new Category(2, "Sport"));
        categoriesList.add(new Category(3, "Health"));
        categoriesList.add(new Category(4, "Politics"));

        onLogin();
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            closeKeyboard();
        } else {
            super.onBackPressed();
        }

        return true;
    }

    private void switchFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }

        transaction.replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName());
        transaction.commit();
        closeKeyboard();
    }

    public void copyToClipboard(String text) {
        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        android.content.ClipData clip = android.content.ClipData.newPlainText(getString(R.string.general_clipboard_copied_label), text);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(getApplicationContext(), R.string.general_clipboard_copied, Toast.LENGTH_SHORT).show();
    }

    public void closeKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    @Override
    public ArrayList<Category> getCategoriesList() {
        return categoriesList;
    }

    @Override
    public ArrayList<Article> getArticlesList() {
        return articlesList;
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void loadArticles() {
        final HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentByTag(HomeFragment.class.getSimpleName());

        GsonRequest<Article[]> request = new GsonRequest<>(this, Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_articles), Article[].class, new Response.Listener<Article[]>() {
            @Override
            public void onResponse(Article[] articles) {
                articlesList.clear();
                articlesList.addAll(Arrays.asList(articles));
                homeFragment.displayArticles();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(getApplicationContext(), error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void loadCategories() {
        GsonRequest<Category[]> request = new GsonRequest<>(this, Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_categories), Category[].class, new Response.Listener<Category[]>() {
            @Override
            public void onResponse(Category[] categories) {
                categoriesList.clear();
                categoriesList.addAll(Arrays.asList(categories));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(getApplicationContext(), error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void loadArticle(Article article) {
        final ArticleFragment articleFragment = (ArticleFragment) getSupportFragmentManager().findFragmentByTag(ArticleFragment.class.getSimpleName());

        GsonRequest<Article> request = new GsonRequest<>(this, Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_article, article.getId()), Article.class, new Response.Listener<Article>() {
            @Override
            public void onResponse(Article articleResponse) {
                articleFragment.article = articleResponse;
                articleFragment.displayArticle();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(getApplicationContext(), error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void loadArticleComments(Article article, final boolean scrollToLast) {
        final CommentFragment commentFragment = (CommentFragment) getSupportFragmentManager().findFragmentByTag(CommentFragment.class.getSimpleName());

        GsonRequest<Comment[]> request = new GsonRequest<>(this, Request.Method.GET, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_article_comments, article.getId()), Comment[].class, new Response.Listener<Comment[]>() {
            @Override
            public void onResponse(Comment[] comments) {
                commentFragment.article.getComments().clear();
                commentFragment.article.getComments().addAll(Arrays.asList(comments));
                commentFragment.displayArticleComments(scrollToLast);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(getApplicationContext(), error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    private void onLogin() {
        SharedPreferences settings = getSharedPreferences(getString(R.string.general_preferences), Context.MODE_PRIVATE);
        String username = settings.getString(getString(R.string.general_preferences_username), "");
        String password = settings.getString(getString(R.string.general_preferences_password), "");
        boolean autoConnect = settings.getBoolean(getString(R.string.general_preferences_autoconnect), false);

        loadCategories();
        setContentView(R.layout.activity_main);

        if (autoConnect && !username.isEmpty() && !password.isEmpty()) {
            onLoginButtonPressed(new User(username, password), true);
        } else {
            LoginFragment loginFragment = LoginFragment.newInstance(username, password);
            switchFragment(loginFragment, false);
        }
    }

    @Override
    public void onLogout() {
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        closeKeyboard();

        SharedPreferences settings = getSharedPreferences(getString(R.string.general_preferences), Context.MODE_PRIVATE);
        String username = settings.getString(getString(R.string.general_preferences_username), "");
        String password = settings.getString(getString(R.string.general_preferences_password), "");

        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(getString(R.string.general_preferences_autoconnect), false);
        editor.remove(getString(R.string.general_preferences_token));
        editor.apply();

        LoginFragment loginFragment = LoginFragment.newInstance(username, password);
        switchFragment(loginFragment, false);
    }

    @Override
    public void onArticleSelected(Article article, int position) {
        ArticleFragment articleFragment = ArticleFragment.newInstance(article, position);
        switchFragment(articleFragment, true);
    }

    @Override
    public void onEditArticleButtonPressed(Article article, int position) {
        ArticleEditFragment articleEditFragment = ArticleEditFragment.newInstance(article, position);
        switchFragment(articleEditFragment, true);
    }

    @Override
    public void onDeleteArticleButtonPressed(final Article article) {
        final HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentByTag(HomeFragment.class.getSimpleName());

        GsonRequest<Article> request = new GsonRequest<>(this, Request.Method.DELETE, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_article, article.getId()), Article.class, new Response.Listener<Article>() {
            @Override
            public void onResponse(Article responseArticle) {
                articlesList.remove(article);
                homeFragment.articlesListAdapter.notifyDataSetChanged();
                FirebaseMessaging.getInstance().unsubscribeFromTopic("Article" + article.getId());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(getApplicationContext(), error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void onCreateArticleButtonPressed() {
        ArticleCreateFragment articleCreateFragment = ArticleCreateFragment.newInstance();
        switchFragment(articleCreateFragment, true);
    }

    @Override
    public void onPostArticleButtonPressed(Article article) {
        final HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentByTag(HomeFragment.class.getSimpleName());

        HashMap<String, String> data = new HashMap<>();
        data.put("title", article.getTitle());
        data.put("headlines", article.getHeadlines());
        data.put("content", article.getContent());
        data.put("categoryId", String.valueOf(article.getCategory().getId()));
        data.put("pictureUrl", article.getPictureUrl());

        GsonRequest<Article> request = new GsonRequest<>(this, Request.Method.POST, new JSONObject(data), getString(R.string.general_api_url) + getString(R.string.general_api_url_articles), Article.class, new Response.Listener<Article>() {
            @Override
            public void onResponse(Article article) {
                article.setUser(new User(getSharedPreferences(getString(R.string.general_preferences), Context.MODE_PRIVATE).getInt(getString(R.string.general_preferences_id), 0), getSharedPreferences(getString(R.string.general_preferences), Context.MODE_PRIVATE).getString(getString(R.string.general_preferences_username), ""), ""));
                articlesList.add(article);
                homeFragment.articlesListAdapter.notifyDataSetChanged();
                getSupportFragmentManager().popBackStack();
                closeKeyboard();
                FirebaseMessaging.getInstance().subscribeToTopic("Article" + article.getId());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(getApplicationContext(), error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void onArticleEditConfirm(Article article, final int position) {
        final HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentByTag(HomeFragment.class.getSimpleName());
        final ArticleFragment articleFragment = (ArticleFragment) getSupportFragmentManager().findFragmentByTag(ArticleFragment.class.getSimpleName());

        HashMap<String, String> data = new HashMap<>();
        data.put("title", article.getTitle());
        data.put("headlines", article.getHeadlines());
        data.put("content", article.getContent());
        data.put("categoryId", String.valueOf(article.getCategory().getId()));
        data.put("pictureUrl", article.getPictureUrl());

        GsonRequest<Article> request = new GsonRequest<>(this, Request.Method.PUT, new JSONObject(data), getString(R.string.general_api_url) + getString(R.string.general_api_url_article, article.getId()), Article.class, new Response.Listener<Article>() {
            @Override
            public void onResponse(Article article) {
                articlesList.set(position, article);
                homeFragment.articlesListAdapter.notifyDataSetChanged();

                if (articleFragment != null) {
                    articleFragment.article = article;
                }

                getSupportFragmentManager().popBackStack();
                closeKeyboard();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(getApplicationContext(), error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void onCommentButtonPressed(Article article) {
        CommentFragment commentFragment = CommentFragment.newInstance(article, false);
        switchFragment(commentFragment, true);
    }

    @Override
    public void onArticleVote(final Article article, int note) {
        final ArticleFragment articleFragment = (ArticleFragment) getSupportFragmentManager().findFragmentByTag(ArticleFragment.class.getSimpleName());

        HashMap<String, String> data = new HashMap<>();
        data.put("note", String.valueOf(note));

        GsonRequest<Evaluation> request = new GsonRequest<>(this, Request.Method.POST, new JSONObject(data), getString(R.string.general_api_url) + getString(R.string.general_api_url_article_evaluate, articleFragment.article.getId()), Evaluation.class, new Response.Listener<Evaluation>() {
            @Override
            public void onResponse(Evaluation evaluation) {
                loadArticle(article);
                logFirebaseArticleVoteEvent(article, evaluation);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(getApplicationContext(), error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void onPostCommentButtonPressed(Comment comment) {
        final CommentFragment commentFragment = (CommentFragment) getSupportFragmentManager().findFragmentByTag(CommentFragment.class.getSimpleName());

        HashMap<String, String> data = new HashMap<>();
        data.put("content", comment.getContent());

        GsonRequest<Comment> request = new GsonRequest<>(this, Request.Method.POST, new JSONObject(data), getString(R.string.general_api_url) + getString(R.string.general_api_url_article_comments, commentFragment.article.getId()), Comment.class, new Response.Listener<Comment>() {
            @Override
            public void onResponse(Comment comment) {
                comment.setUser(new User(getSharedPreferences(getString(R.string.general_preferences), Context.MODE_PRIVATE).getInt(getString(R.string.general_preferences_id), 0), getSharedPreferences(getString(R.string.general_preferences), Context.MODE_PRIVATE).getString(getString(R.string.general_preferences_username), ""), ""));
                commentFragment.article.getComments().add(comment);
                commentFragment.displayArticleComments(true);
                closeKeyboard();
                FirebaseMessaging.getInstance().subscribeToTopic("Article" + commentFragment.article.getId());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(getApplicationContext(), error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void onDeleteCommentButtonPressed(final Comment comment) {
        final CommentFragment commentFragment = (CommentFragment) getSupportFragmentManager().findFragmentByTag(CommentFragment.class.getSimpleName());

        GsonRequest<Comment> request = new GsonRequest<>(this, Request.Method.DELETE, null, getString(R.string.general_api_url) + getString(R.string.general_api_url_article_comment, commentFragment.article.getId(), comment.getId()), Comment.class, new Response.Listener<Comment>() {
            @Override
            public void onResponse(Comment responseComment) {
                commentFragment.article.getComments().remove(comment);
                commentFragment.displayArticleComments(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(getApplicationContext(), error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void onEditCommentButtonPressed(Comment comment, int position) {
        CommentEditFragment commentEditFragment = CommentEditFragment.newInstance(comment, position);
        switchFragment(commentEditFragment, true);
    }

    @Override
    public void onCommentVote(final Comment comment, int note) {
        final CommentFragment commentFragment = (CommentFragment) getSupportFragmentManager().findFragmentByTag(CommentFragment.class.getSimpleName());

        HashMap<String, String> data = new HashMap<>();
        data.put("note", String.valueOf(note));

        GsonRequest<Evaluation> request = new GsonRequest<>(this, Request.Method.POST, new JSONObject(data), getString(R.string.general_api_url) + getString(R.string.general_api_url_article_comment_evaluate, commentFragment.article.getId(), comment.getId()), Evaluation.class, new Response.Listener<Evaluation>() {
            @Override
            public void onResponse(Evaluation evaluation) {
                loadArticleComments(commentFragment.article, false);
                logFirebaseCommentVoteEvent(comment, evaluation);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(getApplicationContext(), error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void onCommentEditConfirm(Comment comment, final int position) {
        final CommentFragment commentFragment = (CommentFragment) getSupportFragmentManager().findFragmentByTag(CommentFragment.class.getSimpleName());

        HashMap<String, String> data = new HashMap<>();
        data.put("content", comment.getContent());

        GsonRequest<Comment> request = new GsonRequest<>(this, Request.Method.PUT, new JSONObject(data), getString(R.string.general_api_url) + getString(R.string.general_api_url_article_comment, commentFragment.article.getId(), comment.getId()), Comment.class, new Response.Listener<Comment>() {
            @Override
            public void onResponse(Comment comment) {
                commentFragment.article.getComments().set(position, comment);
                ((CommentListAdapter) commentFragment.getListAdapter()).notifyDataSetChanged();
                getSupportFragmentManager().popBackStack();
                closeKeyboard();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(getApplicationContext(), error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void onLoginButtonPressed(final User user, final boolean redirectLogin) {
        HashMap<String, String> data = new HashMap<>();
        data.put("username", user.getUsername());
        data.put("password", user.getPassword());

        GsonRequest<Auth> request = new GsonRequest<>(this, Request.Method.POST, new JSONObject(data), getString(R.string.general_api_url) + getString(R.string.general_api_url_auth_login), Auth.class, new Response.Listener<Auth>() {
            @Override
            public void onResponse(Auth auth) {
                SharedPreferences settings = getSharedPreferences(getString(R.string.general_preferences), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(getString(R.string.general_preferences_token), auth.getToken());
                editor.putInt(getString(R.string.general_preferences_id), auth.getUser().getId());
                editor.putString(getString(R.string.general_preferences_username), auth.getUser().getUsername());
                editor.putString(getString(R.string.general_preferences_password), user.getPassword());
                editor.putBoolean(getString(R.string.general_preferences_autoconnect), true);
                editor.apply();

                currentUser = auth.getUser();

                String navigate = getIntent().getStringExtra(EXTRA_NAVIGATE);
                boolean scrollToLast = getIntent().getBooleanExtra(CommentFragment.ARG_SCROLL_TO_LAST, false);
                if (navigate != null) {
                    if (navigate.equals(CommentFragment.class.getSimpleName())) {
                        CommentFragment commentFragment = CommentFragment.newInstance((Article) getIntent().getParcelableExtra(CommentFragment.ARG_ARTICLE), scrollToLast);
                        switchFragment(commentFragment, false);
                    } else if (navigate.equals(ArticleFragment.class.getSimpleName())) {
                        ArticleFragment articleFragment = ArticleFragment.newInstance(getIntent().getIntExtra(ArticleFragment.ARG_ARTICLE_ID, -1), -1);
                        switchFragment(articleFragment, false);
                    }
                } else {
                    HomeFragment homeFragment = new HomeFragment();
                    switchFragment(homeFragment, false);
                }

                FirebaseMessaging.getInstance().subscribeToTopic("ArticlesFeed");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (redirectLogin) {
                    LoginFragment loginFragment = LoginFragment.newInstance(user.getUsername(), user.getPassword());
                    switchFragment(loginFragment, false);
                }

                handleApiError(getApplicationContext(), error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void onRegisterButtonPressed(final User user) {
        HashMap<String, String> data = new HashMap<>();
        data.put(getString(R.string.general_preferences_username), user.getUsername());
        data.put(getString(R.string.general_preferences_password), user.getPassword());

        GsonRequest<User> request = new GsonRequest<>(this, Request.Method.POST, new JSONObject(data), getString(R.string.general_api_url) + getString(R.string.general_api_url_auth_register), User.class, new Response.Listener<User>() {
            @Override
            public void onResponse(User usr) {
                onLoginButtonPressed(user, false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleApiError(getApplicationContext(), error);
            }
        });

        ApiRequestManager.getInstance(this).getRequestQueue().add(request);
    }

    public static void handleApiError(Context context, VolleyError error) {
        String body;

        if (error.networkResponse != null) {
            try {
                body = new String(error.networkResponse.data, Tools.ENCODING_UTF8);
                Toast.makeText(context, "Error " + error.networkResponse.statusCode + ": " + body, Toast.LENGTH_SHORT).show();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(context, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void logFirebaseCommentVoteEvent(Comment comment, Evaluation evaluation) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "comment-" + String.valueOf(comment.getId()));
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, String.valueOf(comment.getContent()));
        bundle.putString(FirebaseAnalytics.Param.SCORE, String.valueOf(evaluation.getNote()));
        FirebaseAnalytics.getInstance(this).logEvent(FirebaseAnalytics.Event.POST_SCORE, bundle);
    }

    private void logFirebaseArticleVoteEvent(Article article, Evaluation evaluation) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "article-" + String.valueOf(article.getId()));
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, String.valueOf(article.getTitle()));
        bundle.putString(FirebaseAnalytics.Param.SCORE, String.valueOf(evaluation.getNote()));
        FirebaseAnalytics.getInstance(this).logEvent(FirebaseAnalytics.Event.POST_SCORE, bundle);
    }
}
