package ovh.bmyguest.baware.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.Arrays;

import ovh.bmyguest.baware.MainActivity;
import ovh.bmyguest.baware.R;
import ovh.bmyguest.baware.network.ApiRequestManager;
import ovh.bmyguest.baware.network.GsonRequest;
import ovh.bmyguest.baware.parcelables.Article;

public class NewsAppWidget extends AppWidgetProvider {
    public static final String EXTRA_IS_MANUAL = "IS_MANUAL";

    public static void updateWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId, boolean notifyDataSetChanged) {
        // ARTICLES LIST
        Intent intent = new Intent(context, NewsAppWidgetService.class);
        Intent startActivityIntent = new Intent(context, MainActivity.class);
        PendingIntent startActivityPendingIntent = PendingIntent.getActivity(context, 0, startActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.news_widget_layout);
        remoteViews.setRemoteAdapter(R.id.widget_list_view_news, intent);
        remoteViews.setEmptyView(R.id.widget_list_view_news, R.id.widget_list_view_empty);
        remoteViews.setPendingIntentTemplate(R.id.widget_list_view_news, startActivityPendingIntent);

        // REFRESH BUTTON
        Intent refreshIntent = new Intent(context, NewsAppWidget.class);
        refreshIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        refreshIntent.putExtra(EXTRA_IS_MANUAL, true);
        refreshIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        PendingIntent refreshPendingIntent = PendingIntent.getBroadcast(context, appWidgetId, refreshIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.widget_refresh_button, refreshPendingIntent);

        // UPDATE
        if (notifyDataSetChanged) {
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.widget_list_view_news);
        }

        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

        if (!intent.getAction().equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
            return;
        }

        if (!intent.getBooleanExtra(EXTRA_IS_MANUAL, false)) {
            return;
        }

        if (appWidgetId == -1) {
            return;
        }

        updateArticlesList(context, appWidgetManager, appWidgetId);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        for (int appWidgetId : appWidgetIds) {
            updateArticlesList(context, appWidgetManager, appWidgetId);
        }
    }

    private void updateArticlesList(final Context context, final AppWidgetManager appWidgetManager, final int appWidgetId) {
        GsonRequest<Article[]> request = new GsonRequest<>(context, Request.Method.GET, null, context.getString(R.string.general_api_url) + context.getString(R.string.general_api_url_articles), Article[].class, new Response.Listener<Article[]>() {
            @Override
            public void onResponse(Article[] articlesResponse) {
                NewsAppWidgetFactory.articles.clear();
                NewsAppWidgetFactory.articles.addAll(Arrays.asList(articlesResponse));
                updateWidget(context, appWidgetManager, appWidgetId, true);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MainActivity.handleApiError(context, error);
            }
        });

        ApiRequestManager.getInstance(context).getRequestQueue().add(request);
    }
}
