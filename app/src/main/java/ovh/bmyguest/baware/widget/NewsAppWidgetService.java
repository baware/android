package ovh.bmyguest.baware.widget;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import android.widget.RemoteViewsService.RemoteViewsFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import ovh.bmyguest.baware.MainActivity;
import ovh.bmyguest.baware.R;
import ovh.bmyguest.baware.fragments.ArticleFragment;
import ovh.bmyguest.baware.parcelables.Article;
import ovh.bmyguest.baware.util.Tools;

public class NewsAppWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new NewsAppWidgetFactory(this.getApplicationContext(), intent);
    }
}

class NewsAppWidgetFactory implements RemoteViewsFactory {
    private Context context;
    static ArrayList<Article> articles = new ArrayList<>();

    NewsAppWidgetFactory(Context context, Intent intent) {
        this.context = context;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {

    }

    @Override
    public void onDestroy() {
        articles.clear();
    }

    @Override
    public int getCount() {
        return articles.size();
    }

    @Override
    public RemoteViews getViewAt(int i) {
        Article article = articles.get(i);

        RemoteViews widgetRow = new RemoteViews(context.getPackageName(), R.layout.news_widget_row);
        widgetRow.setTextViewText(R.id.widget_text_view_article_title, Tools.decodeBase64String(article.getTitle()));
        widgetRow.setTextViewText(R.id.widget_text_view_article_headlines, Tools.decodeBase64String(article.getHeadlines()));
        widgetRow.setTextViewText(R.id.widget_text_view_article_date, new SimpleDateFormat("d MMM", Locale.getDefault()).format(article.getCreatedAt()));
        widgetRow.setTextViewText(R.id.widget_text_view_article_note, context.getString(R.string.fragment_home_article_note, article.getNote()));
        widgetRow.setTextColor(R.id.widget_text_view_article_note, article.getNote() < 0 ? ContextCompat.getColor(context, R.color.vote_red) : article.getNote() > 0 ? ContextCompat.getColor(context, R.color.vote_green) : ContextCompat.getColor(context, R.color.text_color_dark));

        Intent fillInIntent = new Intent();
        fillInIntent.putExtra(MainActivity.EXTRA_NAVIGATE, ArticleFragment.class.getSimpleName());
        fillInIntent.putExtra(ArticleFragment.ARG_ARTICLE_ID, article.getId());
        widgetRow.setOnClickFillInIntent(R.id.widget_text_view_article_container, fillInIntent);

        return widgetRow;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return articles.get(i).getId();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}