package ovh.bmyguest.baware.parcelables;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;

public class Comment implements Parcelable {
    private int id;
    private String content;
    private String pictureUrl;
    private Date createdAt;
    private Date updatedAt;
    private int note;
    private User user;
    private ArrayList<Evaluation> evaluations = new ArrayList<>();

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(content);
        out.writeString(pictureUrl);
        out.writeLong(createdAt.getTime());
        out.writeLong(updatedAt.getTime());
        out.writeInt(note);
        out.writeParcelable(user, flags);
        out.writeTypedList(evaluations);
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel in) {
            return new Comment(in);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };

    private Comment(Parcel in) {
        id = in.readInt();
        content = in.readString();
        pictureUrl = in.readString();
        createdAt = new Date(in.readLong());
        updatedAt = new Date(in.readLong());
        note = in.readInt();
        user = in.readParcelable(User.class.getClassLoader());
        in.readTypedList(evaluations, Evaluation.CREATOR);
    }

    public Comment(String content, String pictureUrl, Date createdAt, Date updatedAt, int note, User user) {
        this.content = content;
        this.pictureUrl = pictureUrl;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.note = note;
        this.user = user;
        this.evaluations = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public int getNote() {
        return this.note;
    }

    public User getUser() {
        return user;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int hasVoted(int userId) {
        if (evaluations == null) {
            return 0;
        }

        for (Evaluation evaluation: this.evaluations) {
            if (evaluation.getUserId() == userId) {
                return evaluation.getNote();
            }
        }

        return 0;
    }
}