package ovh.bmyguest.baware.parcelables;

import android.os.Parcel;
import android.os.Parcelable;

public class Auth implements Parcelable {
    private User user;
    private String token;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeParcelable(user, flags);
        out.writeString(token);
    }

    public static final Creator<Auth> CREATOR = new Creator<Auth>() {
        @Override
        public Auth createFromParcel(Parcel in) {
            return new Auth(in);
        }

        @Override
        public Auth[] newArray(int size) {
            return new Auth[size];
        }
    };

    private Auth(Parcel in) {
        user = in.readParcelable(User.class.getClassLoader());
        token = in.readString();
    }

    public User getUser() {
        return user;
    }

    public String getToken() {
        return token;
    }
}