package ovh.bmyguest.baware.parcelables;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;

public class Article implements Parcelable {
    private int id;
    private String title;
    private String headlines;
    private String content;
    private String pictureUrl;
    private Date createdAt = new Date();
    private Date updatedAt = new Date();
    private int note;
    private User user;
    private Category category;
    private ArrayList<Comment> comments = new ArrayList<>();
    private ArrayList<Evaluation> evaluations = new ArrayList<>();

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(title);
        out.writeString(headlines);
        out.writeString(content);
        out.writeString(pictureUrl);
        out.writeLong(createdAt.getTime());
        out.writeLong(updatedAt.getTime());
        out.writeInt(note);
        out.writeParcelable(user, flags);
        out.writeParcelable(category, flags);
        out.writeTypedList(comments);
        out.writeTypedList(evaluations);
    }

    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    private Article(Parcel in) {
        id = in.readInt();
        title = in.readString();
        headlines = in.readString();
        content = in.readString();
        pictureUrl = in.readString();
        createdAt = new Date(in.readLong());
        updatedAt = new Date(in.readLong());
        note = in.readInt();
        user = in.readParcelable(User.class.getClassLoader());
        category = in.readParcelable(Category.class.getClassLoader());
        in.readTypedList(comments, Comment.CREATOR);
        in.readTypedList(evaluations, Evaluation.CREATOR);
    }

    public Article() {

    }

    public Article(String title, String headlines, String content, String pictureUrl, Date createdAt, Date updatedAt, int note, User user, Category category) {
        this.title = title;
        this.headlines = headlines;
        this.content = content;
        this.pictureUrl = pictureUrl;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.note = note;
        this.user = user;
        this.category = category;
        this.comments = new ArrayList<>();
        this.evaluations = new ArrayList<>();
    }

    public Article(int id, String title, String headlines, String content, String pictureUrl, Date createdAt, Date updatedAt, int note, User user, Category category) {
        this.id = id;
        this.title = title;
        this.headlines = headlines;
        this.content = content;
        this.pictureUrl = pictureUrl;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.note = note;
        this.user = user;
        this.category = category;
        this.comments = new ArrayList<>();
        this.evaluations = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getHeadlines() {
        return headlines;
    }

    public String getContent() {
        return content;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public User getUser() {
        return user;
    }

    public Category getCategory() {
        return category;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public ArrayList<Evaluation> getEvaluations() {
        return this.evaluations;
    }

    public int getNote() {
        return this.note;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setHeadlines(String headlines) {
        this.headlines = headlines;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int hasVoted(int userId) {
        if (evaluations == null) {
            return 0;
        }

        for (Evaluation evaluation: this.evaluations) {
            if (evaluation.getUserId() == userId) {
                return evaluation.getNote();
            }
        }

        return 0;
    }
}