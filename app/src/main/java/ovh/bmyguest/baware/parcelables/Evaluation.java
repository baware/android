package ovh.bmyguest.baware.parcelables;

import android.os.Parcel;
import android.os.Parcelable;

public class Evaluation implements Parcelable {
    private int id;
    private int note;
    private int userId;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeInt(note);
        out.writeInt(userId);
    }

    public static final Creator<Evaluation> CREATOR = new Creator<Evaluation>() {
        @Override
        public Evaluation createFromParcel(Parcel in) {
            return new Evaluation(in);
        }

        @Override
        public Evaluation[] newArray(int size) {
            return new Evaluation[size];
        }
    };

    private Evaluation(Parcel in) {
        id = in.readInt();
        note = in.readInt();
        userId = in.readInt();
    }

    public int getNote() {
        return note;
    }

    int getUserId() {
        return this.userId;
    }

    @Override
    public String toString() {
        return String.valueOf(this.note);
    }
}